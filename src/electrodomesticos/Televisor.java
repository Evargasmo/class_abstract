/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package electrodomesticos;

import java.util.Scanner;

public class Televisor extends Electrodomesticos {
    
     private int pulgadas;
     private String pantalla;
     private int encendido, apagado;
     private int opcion, opcioncanal, opcionvolumen, opcionbrillo, opcioncontraste;
     Scanner leer = new Scanner(System.in);
     
     //constructor
    public Televisor(int pulgadas, String pantalla, String color, double precio, String marca) {
        super(color, precio, marca);
        this.pulgadas=pulgadas;
        this.pantalla=pantalla;
    }

    public int getPulgadas() {
        return pulgadas;
    }

    public void setPulgadas(int pulgadas) {
        this.pulgadas = pulgadas;
    }

    public String getPantalla() {
        return pantalla;
    }

    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }

    public int getEncendido() {
        return encendido;
    }

    public void setEncendido(int encendido) {
        this.encendido = encendido;
    }

    public int getApagado() {
        return apagado;
    }

    public void setApagado(int apagado) {
        this.apagado = apagado;
    }

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    public int getOpcioncanal() {
        return opcioncanal;
    }

    public void setOpcioncanal(int opcioncanal) {
        this.opcioncanal = opcioncanal;
    }

    public int getOpcionvolumen() {
        return opcionvolumen;
    }

    public void setOpcionvolumen(int opcionvolumen) {
        this.opcionvolumen = opcionvolumen;
    }

    public int getOpcionbrillo() {
        return opcionbrillo;
    }

    public void setOpcionbrillo(int opcionbrillo) {
        this.opcionbrillo = opcionbrillo;
    }

    public int getOpcioncontraste() {
        return opcioncontraste;
    }

    public void setOpcioncontraste(int opcioncontraste) {
        this.opcioncontraste = opcioncontraste;
    }
    
    //Métodos

    public void Encender(){
         do{
             System.out.println("Si desea prender el Televisor presione 1"); 
             encendido=leer.nextInt();
             if(encendido==1){
                 System.out.println("TV Encendido");
             }else{
                 Encender();
             }
         }while(encendido!=1);
       
    }
    
    public void controlTV(){
        do{
        System.out.println("Para pasar el canal, presione 1");
        System.out.println("Para subir/bajar el volumen presione 2");
        System.out.println("Para subir/bajar el brillo presione 3");
        System.out.println("Para subir/bajar el contraste presione 4");
        System.out.println("Para apagar el Televisor presione 5");
        switch(opcion=leer.nextInt()){
            case 1:
                System.out.println("Por favor ingrese el número del canal a pasar");
                opcioncanal=leer.nextInt();
                System.out.println("Estás viendo el canal "+opcioncanal);
            break;
            case 2:
                System.out.println("Seleccione el volumen que desee, en un rango de 0 a 100");
                opcionvolumen=leer.nextInt();
                if(opcionvolumen<0 || opcionvolumen>100){
                    System.out.println("Recuerde que el rango de volumen es de 0 a 100");
                }else{
                    System.out.println("Volumen a "+opcionvolumen);
                }
            break;    
            case 3:
                System.out.println("Seleccione el brillo que desee, en un rango de 0 a 100");
                opcionbrillo=leer.nextInt();
                if(opcionbrillo<0 || opcionbrillo>100){
                    System.out.println("Recuerde que el rango de brillo es de 0 a 100");
                }else{
                    System.out.println("Brillo a "+opcionbrillo+"%");
                }
            break;
            case 4:
                System.out.println("Seleccione el contraste que desee, en un rango de 0 a 100");
                opcioncontraste=leer.nextInt();
                if(opcioncontraste<0 || opcioncontraste>100){
                    System.out.println("Recuerde que el rango de contraste es de 0 a 100");
                }else{
                    System.out.println("Contraste a "+opcioncontraste+"%");
                }
            break;
            case 5:
                System.out.println("TV apagado");
            break;
        }
        }while(opcion!=5);
    }
    
    @Override
    public String toString(){
        return super.toString()+ "Pulgadas: "+pulgadas+"\n"+"Tipo de pantalla"+pantalla+"\n";
    }
    
}

