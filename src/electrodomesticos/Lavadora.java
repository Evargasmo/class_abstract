/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electrodomesticos;

/**
 *
 * @author Amoshito
 */
public class Lavadora extends Electrodomesticos{
    
    private int peso;

    public Lavadora(int peso, String color, double precio, String marca) {
        super(color, precio, marca);
        this.peso=peso;       
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    @Override
    public String toString(){
        return super.toString()+ "Peso: "+peso+"\n";
    }
    
}




 

