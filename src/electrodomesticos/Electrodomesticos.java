/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electrodomesticos;

/**
 *
 * @author ESTUDIANTE
 */
public abstract class Electrodomesticos {
   private String color;
   private double precio;
   private String marca;

    public Electrodomesticos(String color, double precio, String marca) {
        this.color = color;
        this.precio = precio;
        this.marca = marca;
    }
    public Electrodomesticos()
    {
    
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    public String toString(){
        return "Color: "+color+"\n"
                +"Precio: "+precio+"\n"
                +"Marca: "+marca+"\n";
    }
}




