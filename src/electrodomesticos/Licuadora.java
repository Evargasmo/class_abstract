/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electrodomesticos;

/**
 *
 * @author Amoshito
 */
public class Licuadora extends Electrodomesticos {
    
    private double velocidad;
    
    public Licuadora(double velocidad, String color, double precio, String marca) {
        super(color, precio, marca);
        this.velocidad = velocidad;
    }

    
    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }
   
    @Override
    public String toString(){
        return super.toString()+ "Velocidad: "+velocidad+"\n";
    }
}




 
